package it.elipse.tnt.subco;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import it.elipse.tnt.card.Card;
import it.elipse.tnt.station.Station;

@Entity
@Table(name="subcontractor")
public class SubContractor {

	@Id
	private String id;
	
	@Column(nullable=false)
	private String name;
	
	@ManyToMany(mappedBy="subcontractors",cascade=CascadeType.ALL)
	private Set<Station> stations = new HashSet<Station>();
	
	@OneToMany(mappedBy="subContractor")
	private Set<Card> cards = new HashSet<Card>();
	
	public SubContractor addStation(Station station) {
		if(station != null) {
			this.getStations().add(station);
			station.getSubcontractors().add(this);
		}
		return this;
	}
	public SubContractor removeStation(Station station) {
		if(station != null) {
			this.getStations().remove(station);
			station.getSubcontractors().remove(this);
		}
		return this;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Station> getStations() {
		return stations;
	}

	public void setStations(Set<Station> stations) {
		this.stations = stations;
	}
	
	public Set<Card> getCards() {
		return cards;
	}

	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}

	public SubContractor() {
		
	}

	public SubContractor(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
}