package it.elipse.tnt.subco;

import org.springframework.data.repository.CrudRepository;

public interface SubContractorRepository  extends CrudRepository<SubContractor, String>{

}
