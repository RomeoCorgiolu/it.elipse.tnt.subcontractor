package it.elipse.tnt;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.elipse.tnt.account.UserService;
import it.elipse.tnt.card.CardService;
import it.elipse.tnt.card.KpiService;
import it.elipse.tnt.station.StationService;
import it.elipse.tnt.subco.SubContractorService;

@Service
public class SeedDataService {

	@Autowired
	private UserService userService;
	@Autowired
	private StationService stationService;
	@Autowired
	private SubContractorService subContractorService;
	@Autowired
	private CardService cardService;
	@Autowired
	private KpiService kpiService;
	
	//@Value("#{new Boolean('${project.seedData}')}")
	private boolean seedData = true;
		
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	private void seed() {
		
		if(!seedData) {
			log.info("SeedData disabled");
			return;
		}
		
		/*log.info("SeedData USER");
		userService.seed();
		log.info("SeedData STATION");
		stationService.seed();		
		log.info("SeedData SUBCONTRACTOR");
		subContractorService.seed();
		log.info("SeedData Card");
		cardService.seed();*/
		log.info("SeedData KPI");
		kpiService.seed();
		
		
	}
	
}
