package it.elipse.tnt.station;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="subarea")
public class SubArea {
	@Id
	private String id;
	private String area;
	public enum CODENUM{ 
		E1(AREAENUM.EAST),E2(AREAENUM.EAST),E3(AREAENUM.EAST),HUB(AREAENUM.HUBINTAIR),HUB1(AREAENUM.HUBDOMROADAIR),HUB2(AREAENUM.HUBDOMROADAIR),S1(AREAENUM.SOUTH),S2(AREAENUM.SOUTH),W1(AREAENUM.WEST),W2(AREAENUM.WEST),W3(AREAENUM.WEST);
		private final AREAENUM area;
		enum AREAENUM {
			EAST("East"),HUBINTAIR("Hub AIR Intl"), HUBDOMROADAIR("Hub Road/Air Dom"),SOUTH("South"),WEST("West");
			private final String description;
			
			AREAENUM(String descritpion){
				this.description=descritpion;
			}
			
			public String getDescritpion() {
				return this.description;
			}
		};
		
		CODENUM(AREAENUM area){
			this.area = area;
		}
		public AREAENUM getArea() {
			return this.area;
		}
	};
	
	@OneToMany(mappedBy="subArea")
	private Set<Station> stations = new HashSet<Station>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public SubArea() {
		
	}
	
	public SubArea(CODENUM id) {
		this.id = id.toString();
		this.area = id.getArea().getDescritpion();
	}
}
