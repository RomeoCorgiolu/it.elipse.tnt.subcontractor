package it.elipse.tnt.station;

import org.springframework.data.repository.CrudRepository;

public interface SubAreaRepository extends CrudRepository<SubArea, String> {

}
