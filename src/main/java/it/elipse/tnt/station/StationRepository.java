package it.elipse.tnt.station;

import org.springframework.data.repository.CrudRepository;
public interface StationRepository extends CrudRepository<Station, String> {

}
