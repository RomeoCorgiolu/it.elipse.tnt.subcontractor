package it.elipse.tnt.station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StationController {

	@Autowired
	private StationService stationService;
	
	@RequestMapping(value = "station", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("stations",stationService.findAll());
        return "station/index";
	}
}
