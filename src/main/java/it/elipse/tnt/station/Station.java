package it.elipse.tnt.station;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import it.elipse.tnt.account.Account;
import it.elipse.tnt.card.Card;
import it.elipse.tnt.subco.SubContractor;
@Entity
@Table(name = "station")
public class Station {

	public enum IDDOM{AG,AL,AN,AO,APR,AR,AT,BA,BDG,BG,BIE,BNS,BO1,BS,BUS,BZ,CA,CIV,CN,CO,CS,CT,EMP,FG,FI,FNR,FO,FR,GE2,GR,IM,LCC,LE,LOD,LU,ME,MI10,MN,MNZ,MO,NA1,NC3,NO,OLB,OME,PA1,PC,PD1,PE,PER,PG,PI,PR,PS,PZ,RA,RC,RE,RIM,RM3,RM5,SA,SBT,SI,SO,SP,SS,TA,TN,TO1,TRM,TS,TV,UA1,UD,VA,VE,VI,VR1,VT,ZD1,BO3,NT3};
	
	@Id
	private String id;
	@Column(name="intcode",nullable=false)
	private String intCode;
	@Column(nullable=false)
	private String name;
	@Column(nullable=false)
	private boolean active = true;
	private String latitude;
	private String longitude;
	
	public enum TypeEnum {DEPOT,HUB};
	@Column(name="type")
	private String type;

	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinColumn(name = "subareaid",nullable=false)
	private SubArea subArea;

	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	 @JoinTable(name = "stationaccount",
	 joinColumns = { @JoinColumn(name = "stationid") },
	 inverseJoinColumns = { @JoinColumn(name = "accountid") })
	private Set<Account> accounts =  new HashSet<Account>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	 @JoinTable(name = "stationsubcontractor",
	 joinColumns = { @JoinColumn(name = "stationid") },
	 inverseJoinColumns = { @JoinColumn(name = "subcontractorid") })
	private Set<SubContractor> subcontractors =  new HashSet<SubContractor>();

	@OneToMany(mappedBy="station")
	private Set<Card> cards = new HashSet<Card>();
	
	public Set<SubContractor> getSubcontractors() {
		return subcontractors;
	}

	public void setSubcontractors(Set<SubContractor> subcontractors) {
		this.subcontractors = subcontractors;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntCode() {
		return intCode;
	}

	public void setIntCode(String intCode) {
		this.intCode = intCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public SubArea getSubArea() {
		return subArea;
	}

	public void setSubArea(SubArea subArea) {
		this.subArea = subArea;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;

	}
	
	public Set<Card> getCards() {
		return cards;
	}

	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}

	public Station() {
		
	}
	
	public Station(IDDOM id) {
		this.id = id.toString();
	}
	
	public Station(String id, String intCode, String name, boolean active,TypeEnum type,SubArea subArea) {
		super();
		this.id = id;
		this.intCode = intCode;
		this.name = name;
		this.active = active;
		this.type = type.toString();
		this.subArea = subArea;
	}
}
