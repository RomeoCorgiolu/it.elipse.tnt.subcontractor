package it.elipse.tnt.station;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import it.elipse.tnt.station.Station.IDDOM;
import it.elipse.tnt.station.Station.TypeEnum;
import it.elipse.tnt.station.SubArea.CODENUM;

@Service
public class StationService {

	@Autowired
	private StationRepository stationRepository;
	@Autowired
	private SubAreaRepository subAreaRepository;
	
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	//@Secured(RoleEnum.Constants.ROLE_ADMIN)
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public Iterable<Station> findAll(){
		return stationRepository.findAll();
	}
	
	
	public void seed() {
		
		subAreaRepository.save(new SubArea(CODENUM.E1));
		subAreaRepository.save(new SubArea(CODENUM.E2));
		subAreaRepository.save(new SubArea(CODENUM.E3));
		subAreaRepository.save(new SubArea(CODENUM.HUB));
		subAreaRepository.save(new SubArea(CODENUM.HUB1));
		subAreaRepository.save(new SubArea(CODENUM.HUB2));
		subAreaRepository.save(new SubArea(CODENUM.S1));
		subAreaRepository.save(new SubArea(CODENUM.S2));
		subAreaRepository.save(new SubArea(CODENUM.W1));
		subAreaRepository.save(new SubArea(CODENUM.W2));
		subAreaRepository.save(new SubArea(CODENUM.W3));
		
		stationRepository.save(new Station(IDDOM.AG.toString(),"IAE","AGRIGENTO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.AL.toString(),"QAL","ALESSANDRIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.AN.toString(),"AOI","ANCONA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.AO.toString(),"AOT","AOSTA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.APR.toString(),"QZR","APRILIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.AR.toString(),"QZO","AREZZO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.AT.toString(),"OAT","ASTI",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.BA.toString(),"BRI","BARI",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.BDG.toString(),"IBD","BASSANO DEL GRAPPA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.BG.toString(),"BRG","BERGAMO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.BIE.toString(),"BEA","BIELLA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.BNS.toString(),"OS3","BEINASCO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.BO1.toString(),"BO1","BOLOGNA LAME",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.BS.toString(),"VBS","BRESCIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.BUS.toString(),"IBU","LAINATE",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.BZ.toString(),"ZBN","BOLZANO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.CA.toString(),"CA7","CAGLIARI ELMAS",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.CIV.toString(),"ICV","CIVITANOVA MARCHE",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.CN.toString(),"CUF","CUNEO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.CO.toString(),"ICM","COMO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.CS.toString(),"QCS","COSENZA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.CT.toString(),"CT1","CATANIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.EMP.toString(),"EM1","EMPOLI",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.FG.toString(),"FOG","FOGGIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.FI.toString(),"FI1","FIRENZE CALENZANO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.FNR.toString(),"FN1","FIANO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.FO.toString(),"FOI","FORLI",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.FR.toString(),"QFR","FROSINONE",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.GE2.toString(),"GOA","GENOVA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.GR.toString(),"GRS","GROSSETO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.IM.toString(),"IIM","IMPERIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.LCC.toString(),"ILJ","LECCO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.LE.toString(),"ILC","LECCE",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.LOD.toString(),"ILD","LODI",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.LU.toString(),"ILT","LUCCA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.ME.toString(),"QME","MESSINA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.MI10.toString(),"MM1","MILANO MEGA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.MN.toString(),"IMV","MANTOVA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.MNZ.toString(),"MZ1","MONZA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.MO.toString(),"MDA","MODENA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.NA1.toString(),"NA1","NAPOLI TEVEROLA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.NC3.toString(),"NC3","NAPOLI CASORIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.NO.toString(),"RNV","NOVARA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.OLB.toString(),"OLB","OLBIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.OME.toString(),"IOE","OMEGNA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.PA1.toString(),"PM4","PALERMO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.PC.toString(),"QPZ","PIACENZA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.PD1.toString(),"QPA","PADOVA INTERPORTO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.PE.toString(),"PE3","PESCARA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.PER.toString(),"IPO","PERO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.PG.toString(),"PEG","PERUGIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.PI.toString(),"PSA","PISA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.PR.toString(),"PMF","PARMA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.PS.toString(),"IPS","PESARO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.PZ.toString(),"IPZ","POTENZA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.RA.toString(),"RAN","RAVENNA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.RC.toString(),"REG","REGGIO CALABRIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.RE.toString(),"REM","REGGIO EMILIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.RIM.toString(),"RMI","RIMINI",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.RM3.toString(),"ROM","ROMA CINECITTA",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.RM5.toString(),"RMZ","ROMA EST",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.SA.toString(),"QSR","SALERNO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.SBT.toString(),"ISZ","S. BENEDETTO DEL T.",true,TypeEnum.DEPOT,new SubArea(CODENUM.E3)));
		stationRepository.save(new Station(IDDOM.SI.toString(),"ISU","SIENA MONTERIGGIONI",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.SO.toString(),"OSO","SONDRIO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.SP.toString(),"QLP","LA SPEZIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.W3)));
		stationRepository.save(new Station(IDDOM.SS.toString(),"QSS","SASSARI",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.TA.toString(),"TAR","TARANTO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.TN.toString(),"ZIA","TRENTO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.TO1.toString(),"TO1","TORINO SAN MAURO",true,TypeEnum.DEPOT,new SubArea(CODENUM.W1)));
		stationRepository.save(new Station(IDDOM.TRM.toString(),"ITL","TERMOLI",true,TypeEnum.DEPOT,new SubArea(CODENUM.S1)));
		stationRepository.save(new Station(IDDOM.TS.toString(),"ITC","TRIESTE",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.TV.toString(),"TV1","TREVISO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.UA1.toString(),"UA1","PORTOGRUARO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.UD.toString(),"UDN","UDINE",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.VA.toString(),"QVA","VARESE",true,TypeEnum.DEPOT,new SubArea(CODENUM.W2)));
		stationRepository.save(new Station(IDDOM.VE.toString(),"VE1","VENEZIA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.VI.toString(),"VNZ","VICENZA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E2)));
		stationRepository.save(new Station(IDDOM.VR1.toString(),"VRN","VERONA",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.VT.toString(),"IVT","VITERBO",true,TypeEnum.DEPOT,new SubArea(CODENUM.S2)));
		stationRepository.save(new Station(IDDOM.ZD1.toString(),"ZD1","ZIBIDO",true,TypeEnum.DEPOT,new SubArea(CODENUM.E1)));
		stationRepository.save(new Station(IDDOM.BO3.toString(),"BLQ","BOLOGNA AEROPORTO",true,TypeEnum.HUB,new SubArea(CODENUM.HUB)));
		stationRepository.save(new Station(IDDOM.NT3.toString(),"NT3","NAPOLI TEVEROLA HUB",true,TypeEnum.HUB,new SubArea(CODENUM.HUB1)));

	}
}
