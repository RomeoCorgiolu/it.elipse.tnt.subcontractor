package it.elipse.tnt.config;

import java.nio.file.ProviderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.tnt.connection.SecurityServiceImpl;

import it.elipse.tnt.account.Account;
import it.elipse.tnt.account.RoleEnum;
import it.elipse.tnt.account.UserService;

@Component
public class TntAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private UserService userService;
	
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        
        Account user = (Account)userService.loadUserByUsername(userName);
		
        if (shouldAuthenticateAgainstThirdPartySystem(user, password)) {
            return new UsernamePasswordAuthenticationToken(user, password,user.getAuthorities());
        } else {
        	throw new BadCredentialsException("External system authentication failed");
        }
    }
 
    private boolean shouldAuthenticateAgainstThirdPartySystem(Account account, String password) {
    	
		if(account.getRole().getAuthority().equals(RoleEnum.ADMIN.toString()) || account.getRole().getAuthority().equals(RoleEnum.USER.toString()) ) {
			if(!password.equals(UserService.DEFPASSWORD))
				throw new BadCredentialsException("Wrong Password");
			else {
				return true;
			}
		}
		
		SecurityServiceImpl service = new SecurityServiceImpl();
		try {
	        String result = service.authenticate(account.getId(), password);
			if(!result.startsWith("00")) {
				throw new BadCredentialsException(result);
			}
		} catch (Exception e) {
			throw new ProviderNotFoundException("Impossible connect to authentication provider");
		}
		
		return true;
	}

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}