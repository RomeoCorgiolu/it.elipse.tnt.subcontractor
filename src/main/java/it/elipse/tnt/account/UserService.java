package it.elipse.tnt.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.*;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AccountRepository accountRepository;
	
	public final static String DEFPASSWORD="S2bco2010$"; 

	public void seed() {
		roleRepository.save(new Role(RoleEnum.USER));
		roleRepository.save(new Role(RoleEnum.ADMIN));
		roleRepository.save(new Role(RoleEnum.STATIONMANAGER));
		
		Account user = new Account("user", new Role(RoleEnum.USER));
		user.setPassword(DEFPASSWORD);
		accountRepository.save(user);
		Account admin = new Account("admin", new Role(RoleEnum.ADMIN));
		user.setPassword(DEFPASSWORD);
		accountRepository.save(admin);
		
		accountRepository.save(new Account("W870DEL","Alessandro","Trombetta","atrombetta@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("R444WJQ","Andrea","Conte","aconte@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("G387HKH","Andrea","D'Imporzano","adimporzano@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Z059LTS","Andrea","Vicari","avicari@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("A689SKE","Andrea","Zucchello","azucchello@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("S869VPT","Antonio","Procida","aprocida@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("N695FTJ","Antonio","Zolli","azolli@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("V432SBR","Arcangelo","Corinti","acorinti@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("L355CFJ","Ausonia","Beretta","sberetta@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("T978RTN","Carlo","Del Piano","cdelpiano@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("H387EZB","Carlo","Fagioli","carlo.fagioli@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("T958RTZ","Davide","Dian","ddian@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("R798ZJQ","Emanuela","Bonis","ebonis@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("G345WKG","Ernesto","Rugiano","ernesto.rugiano@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("U429ZLS","Fabio","Adorno","fabio.adorno@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("V467STV","Fabrizio","Ietto","fietto@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("X533QMG","Fabrizio","Scoppa","fscoppa@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("T951LTU","Flavio","Donato","fdonato@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("L785CMJ","Francesco","Mazzardo","fmazzardo@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W575VML","Giacinto","Baldini","giacinto.baldini@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("C646BAY","Giacomo","De Stefanis","giacomo.destefanis@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W875DEL","Giampaolo","Batori","gbatori@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("F706LZH","Gianmarco","Naimi","gnaimi@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("L795CNJ","Gianni","Tosoni","gtosoni@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W571RHL","Giovanni","Garoffolo","ggaroffolo@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("H689ECU","Giuseppe","Fodale","gfodale@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("N795FHF","Giuseppe","Miceli","gmiceli@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("M963VJK","Lazzaro","Addeo","laddeo@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("A989SFM","Leonardo","Faraoni","lfaraoni@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("G387HKE","Luca","Dell'Orto","ldellorto@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Z053KTB","Luca","Gargano","lgargano@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("L805CYC","Luca","Parisi","lparisi@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W173DRL","Luca","Voltolina","lvoltolina@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("X593KGF","Marco","Contini","mcontini@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("A389SFM","Marco","Madile","mmadile@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Q441EYT","Marco","Saviantoni","msaviantoni@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W774REL","Marco","Volpino","mvolpino@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("R982YJQ","Marco","Zoli","mzoli@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("V482SRB","Maria","Virgallito","mvirgallito@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Q435EYC","Mario","Rinaldi","mrinaldi@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("N395FZK","Mario","Saponaro","msaponaro@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("P484CNR","Massimo","Tagliabue","mtagliabue@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("X563XGP","Matteo","Pascucci","mpascucci@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Q452EYW","Michele","Cardone","mcardone@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("S903EPT","Michele","Ciuffi","mciuffi@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("D034CMC","Michele","Napali","mnapali@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("N595FCR","Michele","Pergola","michele.pergola@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Z354MTG","Nuccio","Manicone","nuccio.manicone@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("M969VUA","Paolo","Rosso","prosso@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("W574DJL","Paolo","Silvola","psilvola@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("C646BAQ","Piero","Bigoni","pierobigoni@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("D512CQC","Pierpaolo","Gualini","pgualini@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("K417PTE","Pietro","Schepis","pschepis@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("F706VYK","Riccardo","Rocca","rrocca@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("U425CLE","Roberto","De Luca","rdeluca@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("C632BAV","Roberto","Nadalin","rnadalin@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("D172CMC","Roberto","Pollara","rpollara@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("F706FEL","Roberto","Sgolastra","rsgolastra@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("M965VFU","Salvatore","Mannanici","smannanici@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("D790CMC","Sandro","Rovatti","srovatti@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("G342HKR","Sergio","Galli","sgalli@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Y959TGR","Stefano","Biegi","sbiegi@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("A784SBX","Stefano","Tomei","stomei@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("A984SAD","Vanes","Galantin","vgalantin@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("Z053DTM","Vincenzo","Ferrara","vferrara@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
		accountRepository.save(new Account("V966SEJ","Vincenzo","Piro","vincenzo.piro@tntitaly.it",new Role(RoleEnum.STATIONMANAGER)));
	}
	
	@Override
	public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String username) throws org.springframework.security.core.userdetails.UsernameNotFoundException {
		Account account = accountRepository.findById(username);
		if(account == null) {
			throw new org.springframework.security.core.userdetails.UsernameNotFoundException("user not found");
		}
		return account;
	}
	
	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(authenticate(account));
	}
	
	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());		
	}	
}
