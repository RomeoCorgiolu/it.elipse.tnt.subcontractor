package it.elipse.tnt.account;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class RoleRepository{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public Role save(Role role) {
		entityManager.merge(role);
		return role;
	}
}
