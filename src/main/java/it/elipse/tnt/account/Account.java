package it.elipse.tnt.account;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import it.elipse.tnt.station.Station;

@Entity
@Table(name = "account")
@NamedQuery(name = Account.FIND_BY_ID, query = "select a from Account a where a.id = :id")
public class Account implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9166449111775189259L;

	public static final String FIND_BY_ID = "Account.findById";

	@Id
	private String id;
	
	@JsonIgnore
	private String password = "";

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleid",nullable = false)
	private Role role;
	
	@ManyToMany(mappedBy="accounts")
	private Set<Station> stations = new HashSet<Station>();

	private String email;
	@Column(name="firstname")
	private String firstName;
	@Column(name="lastname")
	private String lastName;
 
    /* Spring Security related fields*/
	@JsonIgnore
	@Transient 
    private boolean accountNonExpired = true;
	@JsonIgnore
	@Transient 
    private boolean accountNonLocked = true;
	@JsonIgnore
	@Transient 
    private boolean credentialsNonExpired = true;
	@JsonIgnore
	@Transient 
    private boolean enabled = true;

    public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Set<Station> getStations() {
		return stations;
	}

	public void setStations(Set<Station> stations) {
		this.stations = stations;
	}

	protected Account() {

	}
	
	public Account(String id, Role role) {
		this.id = id;
		this.setRole(role);
	}
	
	public Account(String id, String firstName,String lastName,String email,Role role) {
		this.id = id;
		this.firstName=firstName;
		this.lastName = lastName;
		this.email = email;
		this.setRole(role);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(this.getRole());
	}
	@Override
	public String getUsername() {
		return this.id;
	}
	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}
	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}
	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
}
