package it.elipse.tnt.card;

import org.springframework.data.repository.CrudRepository;

public interface KpiRepository extends CrudRepository<Kpi, Long>{

}
