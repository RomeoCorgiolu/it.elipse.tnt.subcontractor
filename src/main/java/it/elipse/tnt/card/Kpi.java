package it.elipse.tnt.card;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="kpi")
public class Kpi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinColumn(name = "kpiareaid",nullable=false)
	private KpiArea kpiArea;
	
	private String description;
	
	public enum SCORETYPEENUM {BOOLEAN,INTEGER}
	@Column(name="scoretype")
	private String scoreType;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.DETACH)
	@JoinColumn(name = "kpitypeid",nullable=false)
	private KpiType kpiType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public KpiArea getKpiArea() {
		return kpiArea;
	}

	public void setKpiArea(KpiArea kpiArea) {
		this.kpiArea = kpiArea;
	}
	
	public Kpi() {
		
	}
	
	public Kpi(KpiArea.KPIAREA kpiarea,String description,KpiType.TYPEENUM kpiType,SCORETYPEENUM scoreType) {
		this.kpiArea = new KpiArea(kpiarea);
		this.kpiType = new KpiType(kpiType);
		this.description = description;
		this.scoreType = scoreType.toString();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getScoreType() {
		return scoreType;
	}

	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}
}
