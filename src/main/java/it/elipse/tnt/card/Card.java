package it.elipse.tnt.card;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import it.elipse.tnt.station.Station;
import it.elipse.tnt.subco.SubContractor;

@Entity
@Table(name="card")
public class Card {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinColumn(name = "stationid",nullable=false)
	private Station station;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinColumn(name = "subcontractorid",nullable=false)
	private SubContractor subContractor;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	 @JoinTable(name = "cardkpiarea",
	 joinColumns = { @JoinColumn(name = "cardid") },
	 inverseJoinColumns = { @JoinColumn(name = "kpiareaid") })
	Set<KpiArea> kpiAreas = new HashSet<KpiArea>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public SubContractor getSubContractor() {
		return subContractor;
	}

	public void setSubContractor(SubContractor subContractor) {
		this.subContractor = subContractor;
	}

	public Set<KpiArea> getKpiAreas() {
		return kpiAreas;
	}

	public void setKpiAreas(Set<KpiArea> kpiAreas) {
		this.kpiAreas = kpiAreas;
	}

	public Card() {
		
	}
	
	public Card(Station station, SubContractor subContractor,Collection<KpiArea> kpiAreas) {
		this.station = station;
		this.subContractor = subContractor;
		this.kpiAreas = new HashSet<KpiArea>(kpiAreas);
	}
 }
