package it.elipse.tnt.card;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="kpiarea")
public class KpiArea {

	@Id
	private String id;
	@Column(nullable = false)
	private String name;
	
	@ManyToMany(mappedBy="kpiAreas")
	Set<Card> cards = new HashSet<Card>();
	
	@OneToMany(mappedBy="kpiArea",fetch = FetchType.EAGER)
	private Set<Kpi> kpis = new HashSet<Kpi>();

	public enum KPIAREA{ 
		OPS("OPS"),HEALTSEAFTY("HEALT & SEAFTY"),SECURITY("SECURITY"),CUSTOMEREXPERIENCE("CUSTOMER EXPERIENCE"),BRANDING("BRANDING"),GOVERNANCE("GOVERNANCE");
		
		private final String name;
		
		public String getName() {
			return this.name;
		}
		
		KPIAREA(String name){
			this.name = name;
		}
	}
	
	public KpiArea() {
		
	}
	
	public KpiArea(KPIAREA kpiarea) {
		this.id = kpiarea.toString();
		this.name = kpiarea.getName();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Card> getCards() {
		return cards;
	}

	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}
	
	public Set<Kpi> getKpis() {
		return kpis;
	}

	public void setKpis(Set<Kpi> kpis) {
		this.kpis = kpis;
	}

	public KpiArea addCard(Card card) {
		if(card != null) {
			this.getCards().add(card);
			card.getKpiAreas().add(this);
		}
		return this;
	}
	public KpiArea removeCard(Card card) {
		if(card != null) {
			this.getCards().remove(card);
			card.getKpiAreas().remove(this);
		}
		return this;
	}
	
	public static Collection<KpiArea> getCollectionAreas() {
		Set<KpiArea> kpiAreas = new HashSet<KpiArea>();
		kpiAreas.add(new KpiArea(KPIAREA.OPS));
		kpiAreas.add(new KpiArea(KPIAREA.HEALTSEAFTY));
		kpiAreas.add(new KpiArea(KPIAREA.SECURITY));
		kpiAreas.add(new KpiArea(KPIAREA.CUSTOMEREXPERIENCE));
		kpiAreas.add(new KpiArea(KPIAREA.BRANDING));
		kpiAreas.add(new KpiArea(KPIAREA.GOVERNANCE));
		return kpiAreas;
	}
	
}
