package it.elipse.tnt.card;

import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card,Long> {

	Iterable<Card> findByStationId(String id);
}
