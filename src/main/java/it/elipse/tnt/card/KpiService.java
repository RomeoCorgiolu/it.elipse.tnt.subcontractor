package it.elipse.tnt.card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.elipse.tnt.card.Kpi.SCORETYPEENUM;
import it.elipse.tnt.card.KpiArea.KPIAREA;
import it.elipse.tnt.card.KpiType.TYPEENUM;

@Service
public class KpiService {

	@Autowired
	private KpiRepository kpiRepository;
	@Autowired
	private KpiTypeRepository kpiTypeRepository;
	
	public void seed() {
		
		kpiTypeRepository.save(new KpiType(TYPEENUM.PUD));
		kpiTypeRepository.save(new KpiType(TYPEENUM.HND));
		kpiTypeRepository.save(new KpiType(TYPEENUM.PUDHND));
		
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Service level performance (cons handed over vs delivered)",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Out for delivery performance scanning",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Handling productivity",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Handling productivity",TYPEENUM.HND,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Inbound performance scanning",TYPEENUM.HND,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Warehouse check performance scanning",TYPEENUM.HND,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.OPS,"Check & weight cube performance",TYPEENUM.HND,SCORETYPEENUM.INTEGER));
		
		kpiRepository.save(new Kpi(KPIAREA.HEALTSEAFTY,"Incidents reporting",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.HEALTSEAFTY,"Training (Law Compliance & Dangerous Goods)",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.HEALTSEAFTY,"Health & safety policy",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.HEALTSEAFTY,"Health & safety documentation",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.HEALTSEAFTY,"Dangerous goods policy",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		
		kpiRepository.save(new Kpi(KPIAREA.SECURITY,"Losses performance",TYPEENUM.PUDHND,SCORETYPEENUM.INTEGER));
		
		kpiRepository.save(new Kpi(KPIAREA.CUSTOMEREXPERIENCE,"POD quality performance",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.CUSTOMEREXPERIENCE,"Missed pick-up performance",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.CUSTOMEREXPERIENCE,"Customers complaints performance",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.CUSTOMEREXPERIENCE,"Damages performance",TYPEENUM.HND,SCORETYPEENUM.INTEGER));
		
		kpiRepository.save(new Kpi(KPIAREA.BRANDING,"Employees uniform condition",TYPEENUM.PUDHND,SCORETYPEENUM.INTEGER));
		kpiRepository.save(new Kpi(KPIAREA.BRANDING,"Employees badge",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.BRANDING,"Vehicle body condition",TYPEENUM.PUD,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.BRANDING,"Vehicle cleanliness",TYPEENUM.PUD,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.BRANDING,"Vehicle branding",TYPEENUM.PUD,SCORETYPEENUM.INTEGER));
		
		kpiRepository.save(new Kpi(KPIAREA.GOVERNANCE,"Key contact availability & accountability",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		kpiRepository.save(new Kpi(KPIAREA.GOVERNANCE,"Action plan & project management",TYPEENUM.PUDHND,SCORETYPEENUM.BOOLEAN));
		
	}
	
}
