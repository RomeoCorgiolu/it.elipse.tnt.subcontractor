package it.elipse.tnt.card;

import org.springframework.data.repository.CrudRepository;
public interface KpiTypeRepository extends CrudRepository<KpiType, String>{

}
