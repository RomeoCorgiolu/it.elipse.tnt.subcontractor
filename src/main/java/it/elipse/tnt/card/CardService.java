package it.elipse.tnt.card;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.elipse.tnt.card.KpiArea.KPIAREA;
import it.elipse.tnt.station.Station.IDDOM;
import it.elipse.tnt.station.StationRepository;
import it.elipse.tnt.subco.SubContractorRepository;

@Service
public class CardService {

	@Autowired 
	CardRepository cardRepository;
	@Autowired 
	StationRepository stationRepository;
	@Autowired 
	SubContractorRepository subContractorRepository;
	
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public void seed() {
		//cardRepository.save(new Card(stationRepository.findOne(IDDOM.AG.toString()),subContractorRepository.findOne("0004022783"),KpiArea.getCollectionAreas()));
	
		
	}	
}
