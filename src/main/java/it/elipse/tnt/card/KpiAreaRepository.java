package it.elipse.tnt.card;

import org.springframework.data.repository.CrudRepository;

public interface KpiAreaRepository extends CrudRepository<KpiArea, String>{

}
