package it.elipse.tnt.card;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="kpitype")
public class KpiType {
	@Id
	private String id;
	
	public enum TYPEENUM{ PUD,HND,PUDHND };
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public KpiType() {
		
	}
	
	public KpiType(TYPEENUM value) {
		this.id = value.toString();
	}
}
